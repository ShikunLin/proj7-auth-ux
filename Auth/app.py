"""
Author: Shikun Lin
UO CIS322 18F

This is a flask server which can calculate the control times.
And save the data to database. Then, send data from database to
front end and print on web page. Also, it we can get differenr format
data from the database. This app also allow user create account and login

"""
import os
import flask
from flask import Flask, redirect, url_for, request, render_template, flash
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from flask_restful import Api, Resource, reqparse
import logging
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Length, EqualTo
from passlib.apps import custom_app_context as pwd_context
from urllib.parse import urlparse, urljoin
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)
import time




###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.setup_app(app)
###
# Pages
###
class User(UserMixin):
    def __init__(self, active=True):
        self.name = ""
        self.password = ""
        self.id = ""
        self.active = active

    def is_active(self):
        return self.active
@login_manager.user_loader
def load_user(id):
    user = ""
    _items = db.usersdb.find()
    items = [item for item in _items]
    for item in items:
        if id == item["_id"]:
            user = item
            break
    if user != "":
        app.logger.info(user)
        res_user = User()
        res_user.id = id
        res_user.name= user["name"]
        res_user.password = user["password"]
        return res_user
    else:
        return None

login_manager.setup_app(app)
class LoginForm(FlaskForm):
    username = StringField('Username:',
        validators=[InputRequired('A username is required!'),
            Length(min=3, max=15, message='Username should between 3 and 15 characters')])
    password = PasswordField('Password:', validators=[InputRequired('Password is required!'),
        Length(min=3, message='Password should be at least 3 characters')])
    remember = BooleanField('Remember me')

class SignUpForm(FlaskForm):
    username = StringField('New Username:',
        validators=[InputRequired('A username is required!'),
            Length(min=3, max=15, message='Username should between 3 and 15 characters')])
    password = PasswordField('New Password:', validators=[InputRequired('Password is required!'),
        Length(min=3, message='Password should be at least 3 characters'),
        EqualTo('confirm', message='Passwords must match')])
    confirm  = PasswordField('Repeat Password')

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc
def hash_password(password):
    return pwd_context.hash(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

@app.route("/", methods=['GET', 'POST'])
@app.route("/login", methods=['GET', 'POST'])
def login():
    app.logger.debug("Main page entry")
    form = LoginForm()
    if form.validate_on_submit():
        user_name = form.username.data
        user_pass = form.password.data
        user_founded = ""
        id =''
        _items = db.usersdb.find()
        items = [item for item in _items]
        for item in items:
            if user_name == item["name"]:
                user_founded = item
                id = item["_id"]
                break
        if user_founded == "":
            return flask.render_template("user_not_found.html")

        if user_founded != "" and verify_password(user_pass, user_founded["password"]):

            user = User()
            user.name=user_founded["name"]
            user.password=user_founded["password"]
            user.id = id
            login_user(user,remember=form.remember.data)
            next = flask.request.args.get('next')
            if not is_safe_url(next):
                return flask.abort(400)
            return flask.redirect(next or flask.url_for("calc"))
    return flask.render_template('login.html',form = form)


@app.route("/api/register",methods=['GET', 'POST'])
def sign_up():
    form = SignUpForm()
    loginForm = LoginForm()
    if form.validate_on_submit():
        new_username = form.username.data
        new_password = form.password.data
        hashed_password = hash_password(new_password)
        user_exist = False

        _items = db.usersdb.find()
        items = [item for item in _items]
        for item in items:
            if new_username == item["name"]:
                user_exist = True

        app.logger.info(user_exist)
        if verify_password(new_password, hashed_password) and not user_exist:
            new_user = {
                "name": new_username,
                "password": hashed_password
            }
            db.usersdb.insert_one(new_user)


            return flask.render_template("login.html",form = loginForm), 201
        else:
            if user_exist:
                return flask.render_template("user_existed.html")
            return flask.render_template("sign_up.html",form = form), 400



    return flask.render_template('sign_up.html',form = form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route("/calc")
def calc():
    return flask.render_template("calc.html")
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("login")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    #Get the data from front end
    km = request.args.get('km', 999)
    beg_date = request.args.get("beg_date",type = str)
    beg_time = request.args.get("beg_time",type = str)
    distance = request.args.get("distance",type = int)
    location = request.args.get("location",type=str)
    # Create a arrow object
    date_time = arrow.utcnow()
    date = arrow.get(beg_date+" "+beg_time,'YYYY-MM-DD HH:mm').isoformat()
    #Set the value based on front end data
    date_time = arrow.get(date)
    #Set the time zone
    date_time=date_time.replace(tzinfo='US/Pacific')
    date_time = date_time.isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    open_time = acp_times.open_time(km, distance, date_time)
    close_time = acp_times.close_time(km, distance, date_time)
    result = {"open": open_time, "close": close_time}


    return flask.jsonify(result=result)

#Send the json with data into database
@app.route('/_submit', methods=['POST'])
def new():
    locations = request.form.getlist("location")
    distance = request.form.get("distance")
    km = request.form.getlist("km")
    open = request.form.getlist("open",type = str)
    close = request.form.getlist("close",type = str)

    kms = []
    opens = []
    closes = []
    for it in km:
        if len(it) != 0:
            kms.append(it)
    for it in open:
        if len(it) != 0:

            opens.append(it)
    for it in close:
        if len(it) != 0:
            closes.append(it)

    data_saved = ""
    items_size = len(opens)


    for i in range(items_size):
        data_saved = data_saved + ' Location: '+ locations[i] + '<br/>'\
            +'     Brevet distance: ' + str(distance) + '<br/>'\
            + '     KM: ' + kms[i] + '<br/>'\
            + '     Open: ' + opens[i] + '<br/>'\
            + '     Close: '  + closes[i] + '<br/>' + '<br/>'

    if items_size == 0:
        data_saved = ""
    data = {'data':data_saved}
    db.tododb.insert_one(data)
    return flask.render_template('calc.html')

#Display the data which saved in database on webpage
@app.route('/_display',methods=['POST'])
def display():
    #Holder for the data need to transfer to fornt end
    items = [{"data":""}]

    #If the database is not empyty, then get the data from database
    if db.tododb.count() == 0:
        items = [{"data":""}]
    else:
        _items = db.tododb.find()
        items = [item for item in _items]
        #delete the Mongodb id from the data which send to front end
        for item in items:
            del item['_id']
    #rendering todo.html with data
    return render_template('todo.html', items=items)


######Helper functions to help get correct format data######

#Get all open and close as list
def get_all_list(items):
    res_list = []
    res = [item.split() for item in items]

    lens = len(res)
    #replace all <br/> with ""
    for i in range(lens):
        for j in range(len(res[i])):
            res[i][j] = res[i][j].replace('<br/>','')

    #get open times and close times
    for i in range(len(res)):
        for j in range(len(res[i])):
            if res[i][j] == "Open:" or res[i][j] == "Close:":
                if "20%" not in res[i][j+1] and "20%" not in res[i][j+2] and "20%" not in res[i][j+3]:
                    res_list.append(res[i][j]+res[i][j+1]+" "+res[i][j+2]+" "+res[i][j+3])
                    j+=1
    return res_list

#Get open time only from all list
def get_open_list(all_list):
    open_list = []
    for item in all_list:
        if "Open:" in item:
            open_list.append(item)
    return open_list

#Get close times only from all list
def get_close_list(all_list):
    close_list = []
    for item in all_list:
        if "Close:" in item:
            close_list.append(item)
    return close_list

#It can make open times and close in csv format
def get_all_lsit_csv(all_list):
    csv_list = []
    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            csv_list.append(temp[0]+", "+temp[1]+":"+temp[2]+"<br/>")
    return csv_list

#It can make Open time and close time in json format
def get_all_list_json(all_list):
    json_list = []

    for i in range(len(all_list)):
        temp = all_list[i]
        if "Open:" in temp:
            temp = temp.split(":")
            temp2 = all_list[i+1].split(":")
            #make a json for every pair of open time and close time
            json_list.append({
                temp[0]:temp[1]+":"+temp[2],
                temp2[0]:temp2[1]+":"+temp2[2]
                })
            i +=1
    return json_list

#It make open time or close time in json format
def get_open_close_list_json(lst):
    json_list = []

    for i in range(len(lst)):
        temp = lst[i]
        if "Open:" in temp or "Close:" in temp:
            temp = temp.split(":")
            json_list.append({
                temp[0]:temp[1]+":"+temp[2]
                })
    return json_list


#sort key for top argument
def get_open_time(open_time_json):
    return (open_time_json["Open"])
def get_close_time(close_time_json):
    return (close_time_json["Close"])

#listAll Api
class ListAll(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        return all_list

#listOpenOnly Api
class ListAllOpenOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        return open_list

#listCloseOnly APi
class ListAllCloseOnly(Resource):
    def get(self):

        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        close_list = get_close_list(all_list)

        return close_list



#listAll/json or listAll/csv Api
class ListAllFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        if formatted == "csv":
            csv_string = get_all_lsit_csv(all_list)
            return csv_string
        elif formatted == "json":
            json_list = get_all_list_json(all_list)
            return json_list

#listOpenOnly/json and listOpenOnly/csv Api
class ListOpenOnlyFormatted(Resource):
    def get(self,formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]
        all_list = get_all_list(items)
        open_list = get_open_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            open_csv = []
            #Check for top is null or not
            if top:
                open_json = get_open_close_list_json(open_list)
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                for it in res:
                    open_csv.append("Open, "+it["Open"] +"<br/>")
                return open_csv
            else:
                open_csv = get_all_lsit_csv(open_list)
                return open_csv
        elif formatted == "json":
            open_json = get_open_close_list_json(open_list)
            if top:
                open_json.sort(key = get_open_time)
                res = open_json[:int(top)]
                return res
            return open_json

#listCloseOnly/json and listCloseOnly/csv Api
class ListCloseOnlyFormatted(Resource):
    def get(self, formatted):
        items = []
        res_list = []

        if db.tododb.count == 0:
            items = []
        else:
            _items = db.tododb.find()
            items = [item["data"] for item in _items]

        all_list = get_all_list(items)
        close_list = get_close_list(all_list)
        top = request.args.get("top", type = str)
        if formatted == "csv":
            close_csv = []
            #Check top argument is null or not
            if top:
                close_json = get_open_close_list_json(close_list)
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                for it in res:
                    close_csv.append("Close, "+it["Close"]+"<br/>")
                return close_csv
            else:
                close_csv = get_all_lsit_csv(close_list)
                return close_csv
        elif formatted == "json":
            close_json = get_open_close_list_json(close_list)
            if top:
                close_json.sort(key = get_close_time)
                res = close_json[:int(top)]
                return res
            return close_json



#############ADD The API TO Resource#######################
api.add_resource(ListAll,'/listAll')
api.add_resource(ListAllOpenOnly,'/listOpenOnly')
api.add_resource(ListAllCloseOnly,'/listCloseOnly')
api.add_resource(ListAllFormatted,'/listAll/<formatted>')
api.add_resource(ListOpenOnlyFormatted,'/listOpenOnly/<formatted>')
api.add_resource(ListCloseOnlyFormatted,'/listCloseOnly/<formatted>')

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
