# Project 7: Adding authentication and user interface to brevet time calculator service  

Author:Shikun Lin  
Contact address: shikunl@uoregon.edu  

## Description  
This peogram can create a flask project. This project can caculate the the ACP times for control points. The  project allows users save the data (open time, close time, location name, brevet distance, and control point distace) into Mongo database with "Submit" button on homepage. Also, it allows users to display the data which saved in database with "Display" button on homepage. Also, it can save the results of calculation into a database. Users can display the data from the data base. Also, can use API to get the data from database. This project also allow users to register account and login and logout.  

## How to Run 
1. cd ./Auth
2. Copy the credentials file into ./Auth  
3. Using "docker-compose up" to build the project.  
4. Launch  [http://127.0.0.1:5000](http://127.0.0.1:5000/)  using web browser  